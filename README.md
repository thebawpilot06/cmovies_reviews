#  Projet C#: CMovie review 
![alt text](./ProjetECE/Resources/Images/popcorn.png)

## Description

Ce projet vous permet de rechercher des films par leur nom. Les films sont recherchés sur l'[API](https://www.themoviedb.org/documentation/api) de [The Movie DB](https://www.themoviedb.org/?language=fr). 

## Guide d'utilisation

- Créez et placez vous dans un dossier vide qui contiendra le projet.
- Git pull-ez le projet avec la commande ``` git pull https://gitlab.com/thebawpilot06/cmovies_reviews ```.
- Ouvrez le projet dans Visual Studio Code et sélectionnez le dossier intitulé **ProjetECE**.

Vous pouvez maintenant utiliser notre projet !


## Qu'est-il possible de faire sur CMovie Review ?

### Page de recherche
Le projet est une base de recherche de film par nom. La page d'accueil, qui est la page de recherche se présente ainsi : 
![alt text](./Images/search.png)

### Recherche d'un film
Faisons une recherche du film *Intouchable*.

Au moment où vous commencez à saisir le nom d'un film, une multitude de films contenant ce que vous tapez saisissez apparaissent en dessous de la barre de recherche comme suit :
![alt text](./Images/first_search.png)

On peut alors choisir un des films apparaissant dans la liste en appuyant sur le bouton **See More** de l'une des cartes d'un film. Par exemple en ayant saisis uniquement *in*, je choisis *Alice in Wonderland*. Une nouvelle fenêtre s'ouvre avec des informations supplémentaires sur le film :
![alt text](./Images/Alice.png)

Si vous le souhaitez vo pouvez fermer la fenêtre qui s'est ouverte ou alors vous pouvez revenir sur la fenêtre de recherche et vous continuez de saisir *intouchable* :
![alt text](./Images/intouchable_first.png)

Le film recherché apparaît dans une nouvelle fenêtre et vous pouvez afficher ses informations complémentaires :
![alt text](./Images/intouchable_second.png)


En sélectionnant un film sur **See More** on a accès au résumé du film, l'affiche, le nom original, sa date de réalisation ainsi que sa note générale sur 10.

### Recherche non aboutissante

Dans le cas où le film que vous recherchez n'existe pas, le résultat de la recherche suivant s'affiche à l'écran:
 ![alt text](./Images/404.png)



##  Contributeurs

* [@TheBawPilot06](https://gitlab.com/thebawpilot06) ~ Hugo PEYRON

* [@Marsovic](https://github.com/marsovic) ~ Marko ARSOVIC

* [@eloialardet](https://github.com/eloialardet) ~ Eloi ALARDET