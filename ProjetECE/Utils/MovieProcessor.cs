﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ProjetECE.Models;
using System.Web.Script.Serialization;

namespace ProjetECE.Utils
{
    public class MovieProcessor
    {
        private static JavaScriptSerializer _serializer = new JavaScriptSerializer();
        private static string _apiKey = "9380c53f4bcb57607ee6921628b610f2";


        public static async Task<List<Movie>> FetchMovies(string request)
        {
            List<Movie> movies = new List<Movie>();

            string apiQuery = $"https://api.themoviedb.org/3/search/movie?api_key={_apiKey}&query={request}";

            using (HttpResponseMessage response = await APIHelper.GetClient().GetAsync(apiQuery))
            {
                if (response.IsSuccessStatusCode)
                {
                    // We transform our JSON in a kind of Javascript object
                    var res = _serializer.Deserialize<Dictionary<string, dynamic>>(await response.Content.ReadAsStringAsync());

                    try
                    {
                        foreach (var movieDB in res["results"])
                        {
                            int id = movieDB["id"];
                            string title = movieDB["title"];
                            string releaseDate = movieDB["release_date"];
                            float averageNote = (float)movieDB["vote_average"];
                            string overview = movieDB["overview"];
                            string posterRef = movieDB["poster_path"] != null ? $"https://image.tmdb.org/t/p/w500/{movieDB["poster_path"]}" : "/Resources/Images/popcorn.png";


                            Movie movie = new Movie(
                                    id: $"{id}",
                                    title: title,
                                    releaseDate: releaseDate,
                                    averageNote: averageNote,
                                    overview: overview,
                                    pic: posterRef
                                );

                            // We add our newest movie in our storage
                            Storage.AddMovie($"{id}", movie);

                         
                            movies.Add(movie);
                        }

                        return movies;
                    } catch(Exception)
                    {
                        throw new Exception("NO MOVIES FOUND!");
                    }

                } else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }
}
