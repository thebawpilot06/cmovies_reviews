﻿using System;
using System.Collections.Generic;
using ProjetECE.Models;
using System.Text.RegularExpressions;

namespace ProjetECE.Utils
{
    public class Storage
    {
        private static Dictionary<string, Movie> _movies = new Dictionary<string, Movie>();


        public static Movie GetMovieById(string id)
        {
            return _movies[id];
        }


        public static bool MovieExists(string movieID)
        {
            try
            {
                Movie movie = _movies[movieID];

                return true;
            } catch(Exception)
            {
                return false;
            }
        }


        public static List<Movie> SearchMoviesByTitle(string title)
        {
            List<Movie> moviesList = new List<Movie>();

            Regex regex = new Regex($"{title.ToLower()}");

            foreach(Movie movie in _movies.Values)
            {
                if (regex.IsMatch(movie.Title.ToLower()))
                {
                    moviesList.Add(movie);
                }
            }

            return moviesList; 
        }

        public static void AddMovie(string id, Movie movie)
        {
            //We first check that the movie is not already present in the cache
            if(MovieExists(id))
            {
                return;
            }


            _movies.Add(id, movie);
        }




        public static void Clear()
        {
            _movies.Clear();
        }


        public static int Count()
        {
            return _movies.Count;
        }
    }
}
