﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ProjetECE.Utils
{
    public class APIHelper
    {
        private static HttpClient _client;

        public static HttpClient GetClient()
        {
            if (_client == null)
            {
                _client = new HttpClient();
                _client.BaseAddress = new Uri("https://api.themoviedb.org/");
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            }

            return _client;
        }
    }
}
