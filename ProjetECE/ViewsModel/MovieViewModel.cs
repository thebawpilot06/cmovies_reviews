﻿using ProjetECE.Models;

namespace ProjetECE.ViewsModel
{
    class MovieViewModel
    {
        public Movie Movie { get; set; }

        public MovieViewModel(Movie movie) {
            this.Movie = movie;
        
        }

    }
}
