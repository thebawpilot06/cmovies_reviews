﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using ProjetECE.Models;
using ProjetECE.Utils;

namespace ProjetECE.ViewsModel
{
    public class MoviesViewModel: INotifyPropertyChanged
    {
        private ObservableCollection<Movie> _movies;
        public event PropertyChangedEventHandler PropertyChanged;

        public MoviesViewModel()
        {
            _movies = new ObservableCollection<Movie>(); 
        }

        public void NotifyPropertyChanged([CallerMemberName] string str = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(str));
            }
        }

        private void AddSeveralItemsToMovies(List<Movie> listOfMovies)
        {
            foreach(Movie movie in listOfMovies)
            {
                _movies.Add(movie);
            }
        }

        public async Task<ObservableCollection<Movie>> UpdateMovies(string title)
        {
            _movies.Clear();

            List<Movie> listCachedMovies = Storage.SearchMoviesByTitle($"{title}");

            // If the cache owns already potential movies, we display them to the user
            
            if (listCachedMovies.Count != 0)
            {
                AddSeveralItemsToMovies(listCachedMovies);

                return _movies;
            }

            // Sinon on fait un appel a notre serveur
            try {

                List<Movie> moviesFetched = await MovieProcessor.FetchMovies(title);


                AddSeveralItemsToMovies(moviesFetched);
            } catch(Exception)
            {
                // No movies returned by the server
            }

            return _movies;
        }
    }      
}
