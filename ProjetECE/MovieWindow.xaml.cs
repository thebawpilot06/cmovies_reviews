﻿using System;
using System.Windows;
using System.Runtime.CompilerServices;
using System.ComponentModel;

using ProjetECE.Models;
using ProjetECE.ViewsModel;

namespace ProjetECE
{
    public partial class MovieWindow : Window
    {

        public MovieWindow(Movie movie)
        {
            InitializeComponent();

            this.DataContext = new MovieViewModel(movie);
            this.Title = movie.Title;
        }
    }
}
