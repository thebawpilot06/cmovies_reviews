﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ProjetECE.Models
{
    public class Movie: INotifyPropertyChanged
    {
        private string _id;
        private string _title;
        private string _releaseDate;
        private float _averageNote;
        private string _overview;
        private string _pic;

        public event PropertyChangedEventHandler PropertyChanged;

        public string Id
        {
            get
            {
                return this._id;
            }

            set
            {
                if (_id != value)
                {
                    _id = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public string Title
        {
            get
            {
                return this._title;
            }

            set
            {
                if (_title != value)
                {
                    _title = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public string ReleaseDate
        {
            get
            {
                return this._releaseDate;
            }

            set
            {
                if (_releaseDate != value)
                {
                    _releaseDate = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public float AverageNote
        {
            get
            {
                return this._averageNote;
            }

            set
            {
                if (_averageNote != value)
                {
                    _averageNote = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public string Overview
        {
            get
            {
                return this._overview;
            }

            set
            {
                if (_overview != value)
                {
                    _overview = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public string Pic
        {
            get
            {
                return this._pic;
            }

            set
            {
                if (_pic != value)
                {
                    _pic = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public Movie(
                string id,
                string title,
                string releaseDate,
                float averageNote,
                string overview,
                string pic
            )
        {
            this._id = id;
            this._title = title;
            this._releaseDate = releaseDate;
            this._averageNote = averageNote;
            this._overview = overview;
            this._pic = pic;
        }

        public void NotifyPropertyChanged([CallerMemberName] string str = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(str));
            }
        }
    }
}
