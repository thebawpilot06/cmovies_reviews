﻿using ProjetECE.Controls;
using System;
using System.Windows;
using System.Windows.Controls;
using ProjetECE.ViewsModel;
using System.Collections.ObjectModel;
using ProjetECE.Models;
using ProjetECE.Utils;

namespace ProjetECE
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MovieSpinner _movieSpinner;
        private AppLogo _appLogo;
        private WrapPanel _movieCardsPanel;
        private NoMovieFound _noMovieFound;

        public MainWindow()
        {
            InitializeComponent();

            testAsync();
            

            this.ResizeMode = ResizeMode.NoResize;

            // Defining all the events
            searchBox.SearchBoxChangedEvent += OnSearchBoxChange;

            ConfiguringElements();

            AddElementIntoDockPanel(_appLogo);
        }


        public async void testAsync()
        {
            await MovieProcessor.FetchMovies("Fast");
        }

        private void ConfiguringElements()
        {
            _movieSpinner = new MovieSpinner();
            _movieSpinner.VerticalAlignment = VerticalAlignment.Center;

            _appLogo = new AppLogo();
            _appLogo.VerticalAlignment = VerticalAlignment.Center;

            _noMovieFound = new NoMovieFound();
            _noMovieFound.VerticalAlignment = VerticalAlignment.Center;

            _movieCardsPanel = new WrapPanel();
            _movieCardsPanel.Margin = new Thickness(50, 40, 50, 20);
            _movieCardsPanel.HorizontalAlignment = HorizontalAlignment.Center;
        }

        private void OnSearchBoxChange(string newValue)
        {
            // We verify if the research is an empty research
            if (newValue == "")
            {
                RemoveDockPanelElement();
                AddElementIntoDockPanel(_appLogo);

                return;
            }

            // Otherwise we start the research
            RemoveDockPanelElement();
            AddElementIntoDockPanel(_movieSpinner);

            // Now we retrieve the new Data
            MoviesViewModel dataContext = (MoviesViewModel)this.DataContext;

            UpdateMovieCardsSync(dataContext, newValue);
        }

        private void RemoveDockPanelElement()
        {
            UIElementCollection children = dockPanel.Children;

            children.RemoveAt(children.Count - 1);
        }

        private void AddElementIntoDockPanel(UIElement element)
        {
            UIElementCollection children = dockPanel.Children;

            children.Add(element);
        }

        private async void UpdateMovieCardsSync(MoviesViewModel dataContext, string request)
        {
            UpdateMovieCards(await dataContext.UpdateMovies(request));
        }

        private void UpdateMovieCards(ObservableCollection<Movie> movies)
        {
            _movieCardsPanel.Children.Clear();

            // We verify that movies is not empty
            if (movies.Count == 0)
            {
                RemoveDockPanelElement();
                AddElementIntoDockPanel(_noMovieFound);
                return;
            }

            // We add all the movies that match our pattern in our stackpanel
            foreach(Movie movie in movies)
            {
                MovieCard movieCard = new MovieCard();

                movieCard.Id = movie.Id;
                movieCard.Title = movie.Title;
                movieCard.ReleaseDate = movie.ReleaseDate;
                movieCard.Resume = movie.Overview;
                movieCard.Pic = movie.Pic;
                movieCard.OnCardClickedEvent += MovieCard_OnCardClickedEvent;

                _movieCardsPanel.Children.Add(movieCard);
            }
           
            // We delete the previous elements from the dock
            RemoveDockPanelElement();

            //Add we addd the new one
            AddElementIntoDockPanel(_movieCardsPanel);
        }

        private void MovieCard_OnCardClickedEvent(string id)
        {
            /*
             * The purpose of this function is to open a new window showing more information about a movie after the user
             * clicked on the See More button of the given movie.
             */

            // We retrieve our movie from our storage to get all its information
            Movie movie = Storage.GetMovieById(id);

            MovieWindow movieWindow = new MovieWindow(movie);

            movieWindow.Show();
        }
    }
}
