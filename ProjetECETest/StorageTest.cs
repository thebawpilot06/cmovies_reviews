using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjetECE.Utils;
using ProjetECE.Models;
using System.Collections.Generic;

namespace ProjetECETest
{
    [TestClass]
    public class StorageTest
    {
        Movie movie = new Movie(
                "movideID",
                "movieTitle",
                "23/04/12",
                4.5f,
                "test du resume",
                "moviePIC"
            );

        [TestMethod]
        public void SearchMoviesByTitleTest()
        {
            
            List<Movie> movies;
            Storage.AddMovie(movie.Id, movie);

            // We first test that the storage does not contain a movie with a title that contains the x letter
            movies = Storage.SearchMoviesByTitle("x");

            Assert.AreEqual(movies.Count, 0);

            // Maintenant nous verifions que le storage contient bien notre film qui a le mot movie dans son titre
            movies = Storage.SearchMoviesByTitle("movie");

            Assert.AreEqual(movies.Count, 1);

            Storage.Clear();
        }

        [TestMethod]
        public void AddMovie()
        {
            Storage.AddMovie(movie.Id, movie);

            Assert.AreEqual(Storage.Count(), 1);

            // On verifie mtn que le film n'est pas rajoute deux fois
            Storage.AddMovie(movie.Id, movie);

            Assert.AreEqual(Storage.Count(), 1);

            Storage.Clear();
        }

        
        [TestMethod]
        public void GetMovieByIdTest()
        {
            Storage.AddMovie(movie.Id, movie);

            Movie movieTested = Storage.GetMovieById(movie.Id);

            Assert.AreEqual(movieTested.Id, movie.Id);

            Storage.Clear();
        }
    }
}
